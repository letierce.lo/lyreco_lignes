<!DOCTYPE html>

<html>
    <head>
        <!--===============================================================================================-->
			<title>ANNUAIRE TELEPHONIQUE</title>
		<!--===============================================================================================-->
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--===============================================================================================-->	
			<link rel="icon" type="image/png" href="ressources/images/icons/logo.ico"/>
		<!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="ressources/vendor/bootstrap/css/bootstrap.min.css">		
			<link rel="stylesheet" type="text/css" href="ressources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">		
			<link rel="stylesheet" type="text/css" href="ressources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/animate/animate.css">	
			<link rel="stylesheet" type="text/css" href="ressources/vendor/css-hamburgers/hamburgers.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/animsition/css/animsition.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/select2/select2.min.css">	
			<link rel="stylesheet" type="text/css" href="ressources/vendor/daterangepicker/daterangepicker.css">
			<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/css/util.css">
			<link rel="stylesheet" type="text/css" href="ressources/css/main.css">
		<!--===============================================================================================-->
			<script src="ressources/vendor/bootstrap/js/bootstrap.min.js"></script>
		<!--===============================================================================================-->
    </head> 

    <body style="background: url(ressources/images/fond.jpg)">
        <div>
            <center>
                <h1 style="color: white">CR&Eacute;ATION D'UNE NOUVELLE LIGNE</h1>
            </center>
        </div>

        <?php
			include("ressources/identifiants.php");            
		?>

        <br />

        <div class="container">
       		<table class="table table-striped">
			   <tbody>
             		<tr>
                		<td>							
							<form class="well form-horizontal" action="scripts/traitement_ajout.php" method="POST">
								<fieldset>  

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Num&eacute;ro&nbsp;:</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-signal"></i></span>
                                                        <input class="form-control" id="numero" type="text" name="numero" placeholder="Entrer un numero" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Intitul&eacute;&nbsp;:</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                                        <input class="form-control" id="intitule" type="text" name="intitule" placeholder="Entrer un intitule" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Type&nbsp;de&nbsp;t&eacute;l&eacute;phone&nbsp;:</label>
                                                <div class="col-md-10 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-phone"></i></span>
                                                        <select name="typeTel" id="typeTel" class="form-control" style="height: auto">
                                                            <option selected disabled hidden value='' ></option>
                                                                <?php
                                                                    $lieu_req = $db->query('SELECT libelle FROM typetel');
                                                                    
                                                                    while ($donnees2 = $lieu_req->fetch())
                                                                    {
                                                                ?>
                                                            <option value="<?php echo $donnees2['libelle'] ?>" > <?php echo $donnees2['libelle'] ?></option>
                                                                <?php						
                                                                    }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <!-- Titre sous-partie : LIEU -->
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">&nbsp;&nbsp;&nbsp;<div style="color: grey">LIEU DE LA LIGNE TÉLÉPHONIQUE</div></th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                    </table>                                      

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Adresse&nbsp;:</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                        <input class="form-control" id="adresse" type="text" name="adresse" placeholder="Entrer une adresse"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Nom&nbsp;du&nbsp;lieu&nbsp;:</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                                        <input class="form-control" id="nomLieu" type="text" name="nomLieu" placeholder="Entrer un nom de lieu"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Type&nbsp;de&nbsp;lieu&nbsp;:</label>
                                                <div class="col-md-10 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-home"></i></span>
                                                        <select name="typeLieu" id="typeLieu" class="form-control" style="height: auto">
                                                            <option selected disabled hidden value='' ></option>
                                                                <?php
                                                                    $lieu_req = $db->query('SELECT libelle FROM typelieu');
                                                                    
                                                                    while ($donnees2 = $lieu_req->fetch())
                                                                    {
                                                                ?>
                                                            <option value="<?php echo $donnees2['libelle'] ?>" > <?php echo $donnees2['libelle'] ?></option>
                                                                <?php						
                                                                    }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Titre sous-partie : EMAIL -->
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">&nbsp;&nbsp;&nbsp;<div style="color: grey">ADRESSE MAIL</div></th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                    </table> 
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Addresse email :</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                                        <input class="form-control" id="email" type="text" name="email" placeholder="Entrez l'adresse email" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                         <div class="col-12">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Prestataire de l'email:</label>
                                                <div class="col-md-10 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-home"></i></span>
                                                        <select name="typeEmail" id="typeEmail" class="form-control" style="height: auto">
                                                            <option selected disabled hidden value='' ></option>
                                                                <?php
                                                                    $typeEmail_req = $db->query('SELECT * FROM PrestaireEmail');
                                                                    
                                                                    while ($donnees2 = $typeEmail_req->fetch())
                                                                    {
                                                                ?>
                                                            <option value="<?php echo $donnees2['idPrestMail'] ?>" > <?php echo $donnees2['libelle'] ?></option>
                                                                <?php						
                                                                    }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    

                                    <!-- Titre sous-partie : INFORMATIONS -->
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">&nbsp;&nbsp;&nbsp;<div style="color: grey">INFORMATIONS SUPPLÉMENTAIRES</div></th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Lieu&nbsp;d'arriv&eacute;e&nbsp;Orange&nbsp;:</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-road"></i></span>
                                                        <input class="form-control" id="lieuOrange" type="text" name="lieuOrange" placeholder="Lieu d'arrivée Orange" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Informations&nbsp;:</label> 
                                                <div class="col-md-8 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-inbox"></i></span>
                                                        <input class="form-control" id="informations" type="text" name="informations" placeholder="Entrer les informations supplémentaires" required="required"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <br />
                                    <br />                                                                                                                         

                                    <div class="btn-group btn-group-justified">
                                        <!-- Bouton de validation : "AJOUTER" -->
                                        <div class="btn-group">
                                            <button type="submit" id="ajout" name="ajout" class="btn btn-success">AJOUTER</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <!-- Bouton annulation : "ANNULER" -->
                <div class="btn-group">
                    <button type="button" class="btn btn-danger" onclick='location.href="accueil.php"'>RETOUR</a>
                </div>
            </div>
        </div>

    </body>
</html>