<?php

// Entête HTTP en UTF-8
header('content-type: text/plain ; charset=utf-8');

// Connexion à la base de données
require 'scripts/constantes.php';

$cnx_dsn = 'mysql:hosts='.bdd_hote.';port='.bdd_port.';dbname='.bdd_nom.';charset=utf8';
$cnx_bdd = new PDO($cnx_dsn, bdd_util, bdd_mdp);

// Récupération des données
$mot = $_POST['mot'];
$mot = strtolower($mot);

// Requête SQL
$req = 'SELECT numero, nomLieu, intitule '
        . 'FROM lignes'
        . ' WHERE numero LIKE "%'.$mot.'%"'
        . ' OR nomLieu LIKE "%'.$mot.'%"'
        . ' OR intitule LIKE "%'.$mot.'%";';

$result = $cnx_bdd->query($req);

// Traitement des résultats
$etud = array();
$lgn = $result->fetch();

while($lgn)
{
    $etud[] = array('numero'=>$lgn['numero'], 'nom'=>$lgn['nomLieu'], 'intitule'=>$lgn['intitule']);
    $lgn = $result->fetch();
}

// Envoi des données
$reponse = array('etud'=>$etud);
$etud_json = json_encode($reponse);
echo $etud_json;

?>