<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
    
    <head>
        <?php
            //Si le titre est indiqué, on l'affiche entre les balises <title></title>
            echo (!empty($titre))?'<title>'.$titre.'</title>':'<title>ANNUAIRE TELEPHONIQUE</title>';
        ?>
    
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    
        <link rel="stylesheet" media="screen" type="text/css" title="Design" href="css/design.css" />
    </head>

    <?php
        //Attribution des variables de session
        $id=(isset($_SESSION['id']))?(int) $_SESSION['id']:0;
        $pseudo=(isset($_SESSION['login']))?$_SESSION['login']:'';

        //On inclue les 2 pages restantes
        include("./ressources/functions.php");
        include("./ressources/constants.php");
    ?>