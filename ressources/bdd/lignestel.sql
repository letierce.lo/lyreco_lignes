-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 29 mai 2019 à 10:44
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `lignestel`
--

-- --------------------------------------------------------

--
-- Structure de la table `lignes`
--

DROP TABLE IF EXISTS `lignes`;
CREATE TABLE IF NOT EXISTS `lignes` (
  `numero` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'numero de la ligne telephonique',
  `adresse` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'adresse de la ligne telehonique',
  `id_typeLieu` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'type de lieu ou se situe la ligne telephonique',
  `nomLieu` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'nom de lieu ou se situe la ligne telephonique',
  `id_typeTelephone` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'type de telephone',
  `informations` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'informations sur la ligne telephonique',
  `lieuArriveeFT` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'ligne d''arrivee France Telecom',
  `intitule` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'intitule de la ligne telephonique',
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `lignes`
--

INSERT INTO `lignes` (`numero`, `adresse`, `id_typeLieu`, `nomLieu`, `id_typeTelephone`, `informations`, `lieuArriveeFT`, `intitule`) VALUES
('0327261160', 'thun-saint-amand', 'Marly', 'maison antoine', '', 'test', 'garage', 'maison letierce'),
('0327261161', 'rjjzehzheu', 'Villainnes', 'kjfozuhffoahz', '', '3254959', 'zohfozeijor', 'ablois'),
('0628659360', '32 rue de l\'E4', 'Autres', 'E4', 'Ligne mobile', 'infos supplémentaires', 'TEST', 'LorisE4'),
('0628659361', 'Adresse TEST', '', '123456', '', 'aaa', 'AAA', 'aaa'),
('0628659362', '&', 'Autres', 'a', 'Ligne principale', 'a', 'a', 'a'),
('123', '123', 'Marly', '123', 'Ligne principale', '123', '123', '123'),
('12344', '1234555', 'Marly', '123', 'Ligne principale', '123', '123', '123');

-- --------------------------------------------------------

--
-- Structure de la table `mails`
--

DROP TABLE IF EXISTS `mails`;
CREATE TABLE IF NOT EXISTS `mails` (
  `idMail` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `idpresta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMail`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mails`
--

INSERT INTO `mails` (`idMail`, `mail`, `numero`, `idpresta`) VALUES
(1, '13@A.com', '12344', 2);

-- --------------------------------------------------------

--
-- Structure de la table `prestaireemail`
--

DROP TABLE IF EXISTS `prestaireemail`;
CREATE TABLE IF NOT EXISTS `prestaireemail` (
  `idPrestMail` int(11) NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idPrestMail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `prestaireemail`
--

INSERT INTO `prestaireemail` (`idPrestMail`, `libelle`) VALUES
(1, 'Google'),
(2, 'Orange'),
(3, 'SFR');

-- --------------------------------------------------------

--
-- Structure de la table `typelieu`
--

DROP TABLE IF EXISTS `typelieu`;
CREATE TABLE IF NOT EXISTS `typelieu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typelieu`
--

INSERT INTO `typelieu` (`id`, `libelle`) VALUES
(1, 'Marly'),
(2, 'Digoin'),
(3, 'Villainnes'),
(4, 'Autres');

-- --------------------------------------------------------

--
-- Structure de la table `typetel`
--

DROP TABLE IF EXISTS `typetel`;
CREATE TABLE IF NOT EXISTS `typetel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typetel`
--

INSERT INTO `typetel` (`id`, `libelle`) VALUES
(1, 'Ligne principale'),
(2, 'Ligne secondaire'),
(3, 'Ligne externe'),
(4, 'Ligne mobile');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `pass_md5` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `login`, `pass_md5`) VALUES
(1, 'letierce.lo', '5f4dcc3b5aa765d61d8327deb882cf99'),
(2, 'test', 'test');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
