<html lang="fr">

	<head>
		<!--===============================================================================================-->
			<title>ANNUAIRE TELEPHONIQUE</title>
		<!--===============================================================================================-->
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--===============================================================================================-->	
			<link rel="icon" type="image/png" href="ressources/images/icons/logo.ico"/>
		<!--===============================================================================================-->		
			<link rel="stylesheet" type="text/css" href="ressources/css/util.css">
			<link rel="stylesheet" type="text/css" href="ressources/css/main.css">	
			<link rel="stylesheet" type="text/css" href="ressources/vendor/bootstrap/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/css-hamburgers/hamburgers.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">	
		<!--===============================================================================================-->
	</head>

	<body>
	
		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100">
					<div class="login100-pic js-tilt" data-tilt>
						<img src="ressources/images/logo.jpg" alt="IMG">
					</div>

					<?php
						if (!isset($_POST['pseudo'])) //On est dans la page de formulaire
						{
							echo'
							<form class="login100-form validate-form" action="connexion.php" method="POST">
								<span class="login100-form-title" style="color: #2365e0">
									CONNEXION
								</span>

								<div class="wrap-input100 validate-input" data-validate = "Identifiant valide requis !">
									<input class="input100" type="text" name="login" placeholder="Identifiant">
									<span class="focus-input100"></span>
									<span class="symbol-input100">
										<i class="fa fa-user" aria-hidden="true"></i>
									</span>
								</div>

								<div class="wrap-input100 validate-input" data-validate = "Mot de passe requis !">
									<input class="input100" type="password" name="password" placeholder="Mot de passe">
									<span class="focus-input100"></span>
									<span class="symbol-input100">
										<i class="fa fa-lock" aria-hidden="true"></i>
									</span>
								</div>
								
								<div class="container-login100-form-btn">
									<button class="login100-form-btn">
										Connexion
									</button>
								</div>
							</form>'
							;
						}
					?>

				</div>
			</div>
		</div>
		
	<!--===============================================================================================-->	
		<script src="ressources/js/main.js"></script>	
		<script src="ressources/vendor/bootstrap/js/popper.js"></script>
		<script src="ressources/vendor/select2/select2.min.js"></script>	
		<script src="ressources/vendor/tilt/tilt.jquery.min.js"></script>
		<script src="ressources/vendor/jquery/jquery-3.2.1.min.js"></script>	
		<script src="ressources/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script>$('.js-tilt').tilt({scale: 1.1})</script>
	<!--===============================================================================================-->

	</body>

</html>