<?php
    session_start();
    session_destroy();
?>

<html>

    <head>
        <!--===============================================================================================-->
        <title>ANNUAIRE TELEPHONIQUE</title>
		<!--===============================================================================================-->
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--===============================================================================================-->	
			<link rel="icon" type="image/png" href="ressources/images/icons/logo.ico"/>
		<!--===============================================================================================-->		
			<link rel="stylesheet" type="text/css" href="ressources/css/util.css">
			<link rel="stylesheet" type="text/css" href="ressources/css/main.css">	
			<link rel="stylesheet" type="text/css" href="ressources/vendor/bootstrap/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/css-hamburgers/hamburgers.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">	
		<!--===============================================================================================-->
    </head>

    <body style="background: url(ressources/images/fond.jpg)">

        <div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100 p-t-30 p-b-50">
					<span class="login100-form-title p-b-41">
                        Vous &ecirc;tes &agrave; pr&eacute;sent d&eacute;connect&eacute; <br /><br />
                        Cliquez <a href="index.php">ici</a> pour revenir &agrave; la page de connexion
                    </span>
                </div>
			</div>
		</div>

	</body>
	
</html>