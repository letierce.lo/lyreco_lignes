<?php

    $numero = $_GET['numero'];

    try
    {
        // Connexion à la base de données
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        $pdo = new PDO('mysql:host=localhost; dbname=lignesTel', 'root', '', $pdo_options);
        $pdo->query('SET NAMES UTF8');

        // Préparation de la requête d'insertion SQL
        $req = $pdo->prepare('DELETE FROM lignes WHERE numero = :numero');

        //Exécution de la requête SQL
        $req->execute(array(
            'numero' => $numero
        ));

?>
    <html>
        <head>
            <!--===============================================================================================-->
                <title>ANNUAIRE TELEPHONIQUE</title>
            <!--===============================================================================================-->
                <meta charset="UTF-8">
                <meta name="viewport" content="width = device-width, initial-scale = 1">
            <!--===============================================================================================-->
                <link rel="stylesheet" href="../css/bootstrap.min.css">
                <link rel="stylesheet" href="../css/style.css">
            <!--===============================================================================================-->
                <script src="jquery.min.js"></script>
                <script src="bootstrap.min.js"></script>
            <!--===============================================================================================-->
        </head> 

        <body style="background: url(../ressources/images/fond.jpg)">

            <div>
                <center>
                    <h2 style="color: white">VALIDATION DE LA SUPPRESSION</h2>
                </center>
            </div>

            <div class="container">

                <br />                    

                La suppression de la ligne t&eacute;l&eacute;phonique a &eacute;t&eacute; effectu&eacute; !

                <br />
                <br />

                <!-- Bouton accueil : "ACCUEIL" -->
                <div class="form-group">
                    <a class="btn btn-danger" href="../index.php">ACCUEIL</a>
                </div>
            </div>
        </body>
    </html>

<?php
    }
    catch(Exception $e)
    {
        // On affiche le message d'erreur s'il y en a une
        die('<br />Erreur : ' . $e->getMessage());
    }

?>