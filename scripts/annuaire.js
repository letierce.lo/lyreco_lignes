/* Fonction INITIALISATION */

$(document).ready(initialisation);

function initialisation()
{
    $("#resultats").append('<p>Aucun(s) résultat(s)</p>');
    $("#chp_rech").keyup(recherche);
}

/* Fonction RECHERCHE */

function recherche()
{
    /* Vidage du bloc de résultats si le champ est vide */
    if($("#chp_rech").val() == '')
    {
        $("#resultats").empty();
        $("#resultats").append('<p>Aucun(s) résultat(s)</p>');
    }
    else
    {
        /* Données à envoyer */
        var contenu = 'mot='+$("#chp_rech").val();
        
        /* Moteur AJAX */
        $.ajax({
           type: 'POST', 
           url: 'recherche.php',
           data: contenu,
           dataType: "json",
           success: resultats,
           error: erreur
        });
    }
}

// Fonction ERREUR
function erreur(o, t, h)
{
    alert('Erreur AJAX: '+t);
}

// Fonction RESULTATS
function resultats(reponse)
{
    /* Vider le bloc de résultats */
    $("#resultats").empty();

    /* Si il n'y a pas de résultat */
    if(reponse.etud.length == 0)
    {
        $("#resultats").append('<p>Aucun(s) résultat(s)</p>');
    }
    /* Sinon affichage des résultats */
    else
    {
        var div = "<table class='table table-dark'><thead class='thead-dark'><tr><th scope='col'>Num&eacute;ro</th><th width=40% scope='col'>Nom du lieu</th><th width=40% scope='col'>Intitule</a></th></tr><thead>";
        var div2 = "";
        var div3 = "</table>";
    
        for(i = 0; i < reponse.etud.length; i++)
        {
        div2 = div2 + '<tbody><tr><td scope="row"><a href="./infos.php?num='+reponse.etud[i].numero+'">'+reponse.etud[i].numero+'</td><td scope="row"><a href="./infos.php?num='+reponse.etud[i].numero+'">'+reponse.etud[i].nom+'</td><td scope="row"><a href="./infos.php?num='+reponse.etud[i].numero+'">'+reponse.etud[i].intitule+'</td></tr></tbody>'
        }
        $("#resultats").append(div+div2+div3);
    }
}