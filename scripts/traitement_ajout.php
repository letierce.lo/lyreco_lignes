<?php

    // Récupération des valeurs dans les champs
    $numero = $_POST['numero'];
    $adresse = $_POST['adresse'];
    $typeLieu = $_POST['typeLieu'];
    $nomLieu = $_POST['nomLieu'];
    $typeTel = $_POST['typeTel'];
    $informations = $_POST['informations'];
    $lieuOrange = $_POST['lieuOrange'];
    $intitule = $_POST['intitule'];

    $email = $_POST['email'];
    $typeEmail = $_POST['typeEmail'];

    try
    {
        // Connexion à la base de données
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        $pdo = new PDO('mysql:host=localhost; dbname=lignesTel', 'root', '', $pdo_options);
        $pdo->query('SET NAMES UTF8');

        // Préparation de la requête d'insertion SQL
        $req = $pdo->prepare('INSERT INTO lignes(numero, adresse, id_typeLieu, nomLieu, id_typeTelephone, informations, lieuArriveeFT, intitule)
            VALUES(:numero, :adresse, :typeLieu, :nomLieu, :typeTel, :informations, :lieuOrange, :intitule) ');

        $req2 = $pdo->prepare('
        INSERT INTO mails(idMail, mail, numero , idpresta)
        VALUES(DEFAULT, :mail, :numero, :typeEmail) ');
        
        //Exécution de la requête SQL
        $req->execute(array(
            'numero' => $numero,            
            'adresse' => $adresse,
            'typeLieu' => $typeLieu,
            'nomLieu' => $nomLieu,
            'typeTel' => $typeTel,
            'informations' => $informations,
            'lieuOrange' => $lieuOrange,
            'intitule' => $intitule
        ));

        $req2->execute(array(
            'typeEmail' => $typeEmail,
            'mail' => $email,
            'numero' => $numero,              
        ));



?>
        <html>
            <head>
                <!--===============================================================================================-->
                    <title>ANNUAIRE TELEPHONIQUE</title>
                <!--===============================================================================================-->
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                <!--===============================================================================================-->	
                    <link rel="icon" type="image/png" href="../ressources/images/icons/logo.ico"/>
                <!--===============================================================================================-->
                    <link rel="stylesheet" type="text/css" href="../ressources/vendor/bootstrap/css/bootstrap.min.css">		
                    <link rel="stylesheet" type="text/css" href="../ressources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">		
                    <link rel="stylesheet" type="text/css" href="../ressources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
                    <link rel="stylesheet" type="text/css" href="../ressources/vendor/animate/animate.css">	
                    <link rel="stylesheet" type="text/css" href="../ressources/vendor/css-hamburgers/hamburgers.min.css">
                    <link rel="stylesheet" type="text/css" href="../ressources/vendor/animsition/css/animsition.min.css">
                    <link rel="stylesheet" type="text/css" href="../ressources/vendor/select2/select2.min.css">	
                    <link rel="stylesheet" type="text/css" href="../ressources/vendor/daterangepicker/daterangepicker.css">
                    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                    <link rel="stylesheet" type="text/css" href="../ressources/css/util.css">
                    <link rel="stylesheet" type="text/css" href="../ressources/css/main.css">
                <!--===============================================================================================-->
                    <script src="../ressources/vendor/bootstrap/js/bootstrap.min.js"></script>
                <!--===============================================================================================-->
            </head> 

            <body style="background: url(../ressources/images/fond.jpg)">

                <div>
                    <center>
                        <h2 style="color: white">VALIDATION DE L'AJOUT</h2>
                    </center>
                </div>

                <div class="container">

                    <br />

                    La ligne t&eacute;l&eacute;phonique a &eacute;t&eacute; ajout&eacute; avec succ&egrave;s !

                    <br />
                    <br />

                    <!-- Bouton accueil : "ACCUEIL" -->
                    <div class="form-group">
                        <a class="btn btn-danger" href="../accueil.php">ACCUEIL</a>
                    </div>
                </div>
            </body>
        </html>

<?php

    }
    catch (Exception $e)
    {
        // On affiche le message d'erreur s'il y en a une
        die('<br />Erreur : ' . $e->getMessage()); 
    }

?>