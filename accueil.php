<!DOCTYPE html>

<html>
    <head>
    <!--===============================================================================================-->
			<title>ANNUAIRE TELEPHONIQUE</title>
		<!--===============================================================================================-->
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--===============================================================================================-->	
			<link rel="icon" type="image/png" href="ressources/images/icons/logo.ico"/>
		<!--===============================================================================================-->		
			<link rel="stylesheet" type="text/css" href="ressources/css/util.css">
			<link rel="stylesheet" type="text/css" href="ressources/css/main.css">
            <link rel="stylesheet" type="text/css" href="ressources/css/style.css">	
			<link rel="stylesheet" type="text/css" href="ressources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/bootstrap/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/css-hamburgers/hamburgers.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">	
		<!--===============================================================================================-->
            <script type="text/javascript" src="scripts/jquery.min.js"></script>
            <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
            <script type="text/javascript" src="scripts/annuaire.js"></script>
        <!--===============================================================================================-->
    </head> 

    <body style="background: url(ressources/images/fond.jpg)">

        <div style="text-align: center; color: white">
            <h2>ANNUAIRE TELEPHONIQUE LYRECO - FRANCE</h2>
        </div>

        <div class="deco" style="margin-right: 1%; float: right; margin-top: 1%">
            <a style="float: right; margin-right: -1%" onClick="return confirm('&egrave;tes-vous s&ugrave;r de vouloir vous d&eacute;connecter ?')" href="deconnexion.php"><img src="ressources/images/deconnexion.png"></img></a>
        </div>

        <br />

        <!-- Initialisation d'un formulaire -->
        <form class="form-horizontal" action="#" method="POST">
            
            <!-- Champ : saisir un numéro, un nom de lieu, un intitulé -->
            <div class="form-group">
                <center>
                    <label style="color: white; text-decoration: underline" class="control-label col-sm-4 btn btn-lg" for="rech">Recherche&nbsp;:</label>                
                    <input style="width: 95%" type="text" class="form-control" id="chp_rech" name="chp_rech" size = auto />
                </center>
            </div>

            <br />

            <center>
                <div id="resultats" style="width: 100%">
                </div>
            </center>

            <br />

            <div class="form-group">
                <center>
                    <label style="color: white; text-decoration: underline" class="control-label col-sm-4 btn btn-lg" for="ajout">INS&Eacute;RER&nbsp;UNE&nbsp;NOUVELLE&nbsp;LIGNE&nbsp;:</label>
                    <div class="col-sm-2">
                        <input type="button" class="btn btn-success" id="chp_ajout" name="chp_ajout" onclick="location.href='ajout.php'" value="AJOUTER" />
                    </div>
                </center>
            </div>

        </form>

    </body>
</html>