<?php
	session_start();
	include("ressources/identifiants.php");
	include("ressources/debut.php");
?>

<html>

	<head>
		<!--===============================================================================================-->
        <title>ANNUAIRE TELEPHONIQUE</title>
		<!--===============================================================================================-->
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--===============================================================================================-->	
			<link rel="icon" type="image/png" href="ressources/images/icons/logo.ico"/>
		<!--===============================================================================================-->		
			<link rel="stylesheet" type="text/css" href="ressources/css/util.css">
			<link rel="stylesheet" type="text/css" href="ressources/css/main.css">	
			<link rel="stylesheet" type="text/css" href="ressources/vendor/bootstrap/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/vendor/css-hamburgers/hamburgers.min.css">
			<link rel="stylesheet" type="text/css" href="ressources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">	
		<!--===============================================================================================-->
	</head>

	<body>

		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100 p-t-30 p-b-50">

				<?php
				//On reprend la suite du code
				$id=0;
				if ($id!=0) erreur(ERR_IS_CO);

				else
				{
					$message='';
					if (empty($_POST['login']) || empty($_POST['password']) ) //Oublie d'un champ
					{
						$message ='
							<div class="wrap-login100 p-t-30 p-b-50">
								<span class="login100-form-title p-b-41">
									<p>une erreur s\'est produite pendant votre identification.	Vous devez remplir tous les champs</p>
									<p>Cliquez <a href="index.php">ici</a> pour revenir</p>
								</span>
							</div>';
					}
					else //On check le mot de passe
					{		
						$query=$db->prepare('SELECT id, login, pass_md5 FROM utilisateurs WHERE login = :login');
						$query->bindValue(':login',$_POST['login'], PDO::PARAM_STR);
						$query->execute();
						$data=$query->fetch();
						
						if ($data['pass_md5'] == md5($_POST['password'])) // Acces OK + decryptage pour verification!
						{	
							$_SESSION['login'] = $data['login'];
							$_SESSION['id'] = $data['id'];
							$message ='							
								<div class="wrap-login100 p-t-30 p-b-50">
									<span class="login100-form-title p-b-41">
										<p>Bienvenue '.$data['login'].', vous &ecirc;tes maintenant connect&eacute;!</p>
										<p>Vous allez maintenant acc&eacute;der &agrave; l\'espace membre</p>
									</span>
								</div>';  

							header ("Refresh: 5;URL=accueil.php");
						}
						else // Acces pas OK !
						{
							$message = '
								<span class="login100-form-title p-b-41" style="color: white">
									<p>Une erreur s\'est produite pendant votre identification.<br /> 
									   Le mot de passe ou le pseudo entr&eacute; n\'est pas correct.</p>
									<p>Cliquez <a href="index.php">ici</a> pour revenir &agrave; la page d accueil</p>
								</span>';
						}
						$query->CloseCursor();
					}					
					echo $message;
				}
				?>

				</div>
			</div>
		</div>

	</body>
	
</html>